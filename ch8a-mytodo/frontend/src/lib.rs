#[macro_use]
extern crate seed;
// ANCHOR: future
use futures::Future;
// ANCHOR_END: future
use seed::prelude::*;
// ANCHOR: use_seed
use seed::{fetch, Request};
// ANCHOR_END: use_seed

use mytodo::{JsonApiResponse, Task};

#[derive(Clone, Debug)]
enum Direction {
    Coming,
    Going,
}

struct Model {
    direction: Direction,
}

// ANCHOR: msg
#[derive(Clone, Debug)]
enum Msg {
    FetchedTasks(fetch::ResponseDataResult<JsonApiResponse>),
}
// ANCHOR_END: msg

// ANCHOR: update
fn update(msg: Msg, model: &mut Model, orders: &mut impl Orders<Msg>) {
    match msg {
        Msg::FetchedTasks(Ok(result)) => {
            // TODO: update the model
        }
        Msg::FetchedTasks(Err(reason)) => {
            log!(format!("Error fetching: {:?}", reason));
            orders.skip();
        }
    }
}
// ANCHOR_END: update

fn view(model: &Model) -> impl View<Msg> {
    let greeting = match model.direction {
        Direction::Coming => "Hello, World!",
        Direction::Going => "¡Hasta la vista!",
    };
    h1![
        class! {"heading"},
        style!["height" => "100vh",
               "width" => "100vw",
        ],
        { greeting },
    ]
}

// ANCHOR: init
fn fetch_drills() -> impl Future<Item = Msg, Error = Msg> {
    Request::new("http://localhost:8000/tasks/").fetch_json_data(Msg::FetchedTasks)
}

fn init(_url: Url, orders: &mut impl Orders<Msg>) -> Model {
    orders.perform_cmd(fetch_drills());
    Model {
        direction: Direction::Coming,
    }
}
// ANCHOR_END: init

#[wasm_bindgen(start)]
pub fn render() {
    seed::App::build(init, update, view).finish().run();
}
