# Let's Get Started

## Pre-Requisite Packages

In order to install rustup using the instructions on the website, you will need
curl:

```sh
$ sudo apt update
$ sudo apt install curl
```

In order to run the compiler and other tools, we need some basic development
tools installed:

```sh
$ sudo apt install build-essential
```

## Install via rustup

Go to [https://rustup.rs](https://rustup.rs) and follow the instructions. Note that some linux
distributions provide stable rust releases, but for the purposes of what
we're doing here we want to use a nightly toolchain.

Install the nightly toolchain: `rustup toolchain install nightly`.

And then set it to be the global default: `rustup default nightly`.

## Generate Our App

We can verify that our installation was successful and generate the
skeleton for our app:

```sh
$ cargo new mytodo
$ cd mytodo
$ cargo run
```

You should see messages as your app builds, and then the "Hello World"
greeting. If not, go back to [https://rustup.rs](https://rustup.rs) and verify that you've
followed directions correctly.
